#!/usr/bin/env python

from argparse import ArgumentParser

bracket_pairs = {
    ']': '[',
    ')': '(',
    '}': '{'
}


def parse_cmd_args():
    parser = ArgumentParser()
    parser.add_argument('-s', '--check_string',
                        help='string to check')

    args = parser.parse_args()
    return args

def main(**kwargs):
    result = True
    check_str = kwargs.get('check_string', '')
    print(f"Parsing: '{check_str}'") 
    #valid_chars = list(bracket_pairs.keys()) + list(bracket_pairs.values())
    char_stack = []
    for c in check_str:
        if c in bracket_pairs.keys():
            if char_stack[-1] == bracket_pairs[c]:
                char_stack.pop(-1)
            else:
                char_stack.append(c)
        elif c in bracket_pairs.values():
            char_stack.append(c)
        else:
            continue
    
    if len(char_stack):
        result = False
        print(f'Remaining characters: {char_stack}')
    
    print(f"Result of '{check_str}': {result}")

    return result

if __name__ == '__main__':
    args = parse_cmd_args()
    main(**vars(args))

